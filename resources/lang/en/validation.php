<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'name' => [
            'required' => 'Nuk mund ta lesh pershkrimin e mallit nr.1 bosh',

        ],

        'unit' => [
            'required' => 'Nuk mund ta lesh njesine bosh',

        ],
        'unit_price' => [

            'required' => 'Vendos nje cmim baze' ,
            'numeric' => 'Cmimi baze duhet te jete numer',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity' => [
            'required' => 'Nuk mund ta lesh sasine e mallit bosh',
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative',
            'max' => 'Sasia nuk mund te jete nje numer kaq i madh'
        ],
        'buyer_name' => [
            'required' => 'Nuk mund ta lesh emrin e klientit bosh',
            'max' => 'Emri i klientit nuk mund te jete me i gjate se 25 shkronja',
            'alpha' => 'Emri duhet te permbaje vetem shkronja'

        ],
        'buyer_surname' => [
            'required' => 'Nuk mund ta lesh mbiemrin e klientit bosh',
            'max' => 'Mbiemri i klientit nuk mund te jete me i gjate se 25 shkronja',
            'alpha' => 'Mbiemri duhet te permbaje vetem shkronja'
        ],
        'buyer_phone_number' => [
            'required' => 'Nuk mund ta lesh numrin e celularit te klientit bosh',
            'numeric' =>  'Numri i celularit nuk mund te permbaj fjale',
            'max' => 'Numri i celularit nuk mund te permbaj me shume se 12 numra'
        ],
        'buyer_full_name' => [
            'required' => 'Nuk mund ta lesh bleresin pa zgjedhur'
        ],
        'unit_price_without_tax' => [
            'required' => 'Nuk mund ta lesh cmimin per njesi pa TVSH bosh te pakten per mallin e pare',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer'
        ],
        'quantity2' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax2' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity3' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative',

        ],
        'unit_price_without_tax3' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity4' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax4' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity5' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax5' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity6' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax6' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ'
        ],
        'quantity7' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax7' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],
        'quantity8' => [
            'numeric' => 'Sasia duhet te plotesohet si numer',
            'min' => 'Sasia nuk mund te lihet negative'
        ],
        'unit_price_without_tax8' => [
            'numeric' => 'Cmimi per njesi duhet te plotesohet si numer',
            'min' => 'Cmimi per njesi nuk mund te lihet negativ',
            'max' => 'Cmimi nuk mund te jete kaq i larte'
        ],


    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
