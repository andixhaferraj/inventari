@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <h1 class="page-header">Shto Klient Te Ri </h1>

        <div class="col-sm-4">

            {!! Form::open(['method'=>'POST','action'=>'AdminCustomersController@store' , 'files' => true , 'onsubmit' => 'return confirm("Je i sigurt qe do ta krijosh kete klient?")'])!!}

            {!! csrf_field() !!}

            <div class="form-group">
                {!! Form::label('buyer_name','Emri') !!}
                {!! Form::text('buyer_name',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('buyer_surname','Mbiemri') !!}
                {!! Form::text('buyer_surname',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('buyer_phone_number','Numer Celulari') !!}
                {!! Form::text('buyer_phone_number',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('address','Adresa') !!}
                {!! Form::text('address',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('nipt','NIPT') !!}
                {!! Form::text('nipt',null,['class'=>'form-control']) !!}
            </div>



            {{--<div class="form-group">--}}
            {{--{!! Form::file('file',['class'=>'form-control']) !!}--}}
            {{--</div>--}}

            <div class="form-group">

                {!! Form::submit('Kliko per te shtuar klientin' , ['class'=>'btn btn-info']) !!}
            </div>

            {!! Form::close() !!}

        </div>


    </div>


    @include('admin.includes.createFormError')

@endsection