@extends('layouts.admin')

@section('content')

    <div class="container-fluid">


        <h1 class="page-header text-center">Klientet</h1>
        <form action="/searchcustomer" method="POST" role="search" style="width: 310px; height: 70px">
            {{ csrf_field() }}
            <div class="input-group" >
                <input type="text" class="form-control" name="customer_search"
                       placeholder="Kerko klientet sipas emrit ose mbiemrit"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-hover table-condensed ">
                <thead>
                <tr>

                    <th>Nr</th>
                    <th>Emri</th>
                    <th>Mbiemri</th>
                    <th>Numer celulari</th>
                    <th>Adresa</th>
                    <th>NIPT</th>
                    <th></th>
                    <th></th>

                </tr>
                </thead>
                <tbody>

                @if($customers)

                    @foreach($customers as $customer)
                        <tr>
                            <td>{{$nr++}}</td>
                            <td>{{$customer->buyer_name}}</td>
                            <td>{{$customer->buyer_surname}}</td>
                            <td>{{$customer->buyer_phone_number}}</td>
                            <td>{{$customer->address}}</td>
                            <td>{{$customer->nipt}}</td>
                            <td style="width: 50px"><a href="{{route('admin.customers.invoices' , $customer->id)}}" class="btn btn-info" role="button">Shiko Faturat E Klientit</a></td>
                            <td style="width: 50px"><a href="{{route('admin.customers.edit' , $customer->id)}}" class="btn btn-info" role="button">Ndrysho Te Dhenat E Klientit</a></td>
                        </tr>
                    @endforeach

                @endif

                </tbody>
            </table>
            <div style="position: relative;">{{$customers->render()}}</div>
        </div>
    </div>

@endsection