@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <h1 class="page-header">Ndrysho Te Dhenat E Klientit </h1>

        <div class="col-sm-4">

            {!! Form::model($customer , ['method'=>'PATCH','action'=>['AdminCustomersController@update' , $customer->id], 'files' => true , 'onsubmit' => 'return confirm("Je i sigurt qe do ta ndryshosh klientin?")'])!!}

            {!! csrf_field() !!}

            <div class="form-group">
                {!! Form::label('buyer_name','Emri') !!}
                {!! Form::text('buyer_name',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('buyer_surname','Mbiemri') !!}
                {!! Form::text('buyer_surname',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('buyer_phone_number','Numer Celulari') !!}
                {!! Form::text('buyer_phone_number',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('address','Adresa') !!}
                {!! Form::text('address',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('nipt','NIPT') !!}
                {!! Form::text('nipt',null,['class'=>'form-control']) !!}
            </div>



            {{--<div class="form-group">--}}
            {{--{!! Form::file('file',['class'=>'form-control']) !!}--}}
            {{--</div>--}}

            <div class="form-group">

                {!! Form::submit('Kliko per te ndryshuar klientin' , ['class'=>'btn btn-info']) !!}
            </div>

            {!! Form::close() !!}

            {!! Form::open(['method' => 'DELETE' , 'action' => ['AdminCustomersController@destroy', $customer->id] , 'onsubmit' => 'return confirm("Je i sigurt qe do ta fshish kete klient??")'])!!}
            <div class="form-group">

                {!! Form::submit('FSHI' , ['class'=>'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}


        </div>


    </div>


    @include('admin.includes.createFormError')

@endsection