@extends('layouts.admin')

@section('content')

    <h1 class="page-header text-center">Inventari</h1>

    <div class="container-fluid">
        <form action="/searchitem" method="POST" role="search" style="width: 310px; height: 70px">
            {{ csrf_field() }}
            <div class="input-group" >
                <input type="text" class="form-control" name="item_search"
                       placeholder="Kerko produktet sipas pershkrimit"> <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </form>
        <div class="table-responsive">
            <?php $nr = 1; ?>
            <table class="table table-hover ">
                <thead>
                <tr>

                    <th>Nr</th>
                    <th>Pershkrimi i mallit</th>
                    <th>Njesia</th>
                    <th>Cmimi baze</th>
                    <th>Sasia</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                @if($items)

                    @foreach($items as $item)
                        <tr>

                            <td><?php echo $nr++ ?></td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->unit}}</td>
                            <td>{{$item->unit_price}}</td>
                            <td>{{number_format($item->quantity)}}</td>
                            <td style="width: 50px"><a href="{{route('admin.items.edit' , $item->id)}}" class="btn btn-info" role="button">Ndrysho produktin</a></td>
                            {{--<td><button type="button" class="btn btn-primary"><a href="{{route('admin.items.edit' , $item->id)}}">Ndrysho</a></button></td>--}}
                        </tr>
                    @endforeach

                @endif

                </tbody>
            </table>
        </div>
        <div style="position: relative;">{{$items->render()}}</div>
    </div>

    @endsection