@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <h1 class="page-header">Shto Produkt Te Ri</h1>

        <div class="col-sm-4">

            {!! Form::open(['method'=>'POST','action'=>'AdminItemsController@store' , 'files' => true , 'onsubmit' => 'return confirm("Je i sigurt qe do ta shtosh kete produkt ne magazine?")'])!!}

            {!! csrf_field() !!}

            <div class="form-group">
                {!! Form::label('name','Pershkrimi i mallit:') !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('unit','Njesia') !!}
                {!! Form::select('unit', ['kg' => 'kg', 'cop' => 'cop' , 'liter' => 'liter'], null, ['placeholder' => 'Kliko ketu per te zgjedhur njesine']); !!}
            </div>

            <div class="form-group">
                {!! Form::label('unit_price','Cmimi baze') !!}
                {!! Form::text('unit_price',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('quantity','Sasia') !!}
                {!! Form::number('quantity',null,['class'=>'form-control']) !!}
            </div>

            {{--<div class="form-group">--}}
            {{--{!! Form::file('file',['class'=>'form-control']) !!}--}}
            {{--</div>--}}

            <div class="form-group">

                {!! Form::submit('Kliko per te shtuar produktin' , ['class'=>'btn btn-info']) !!}
            </div>

            {!! Form::close() !!}

        </div>


    </div>


    @include('admin.includes.createFormError')

@endsection