@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <h1 class="page-header">Ndrysho produktin</h1>

        <div class="col-sm-4">

            {!! Form::model($item , ['method'=>'PATCH','action'=>['AdminItemsController@update' , $item->id] , 'files' => true , 'onsubmit' => 'return confirm("Je i sigurt qe do ta ndryshosh kete produkt?")'])!!}

            {!! csrf_field() !!}

            <div class="form-group">
                {!! Form::label('name','Pershkrimi i mallit:') !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('unit','Njesia') !!}
                {!! Form::select('unit', ['kg' => 'kg', 'cop' => 'cop' , 'liter' => 'liter'], null, ['placeholder' => 'Zgjidh njesine']); !!}
            </div>

            <div class="form-group">
                {!! Form::label('unit_price','Cmimi baze') !!}
                {!! Form::number('unit_price',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('quantity','Sasia') !!}
                {!! Form::number('quantity',null,['class'=>'form-control']) !!}
            </div>

            {{--<div class="form-group">--}}
            {{--{!! Form::file('file',['class'=>'form-control']) !!}--}}
            {{--</div>--}}

            <div class="form-group">

                {!! Form::submit('Kliko per te ndryshuar produktin' , ['class'=>'btn btn-info']) !!}
            </div>

            {!! Form::close() !!}

        </div>


    </div>


    @include('admin.includes.createFormError')

@endsection