@extends('layouts.admin')


@section('content')
    <div class="alert alert-danger" style="position:relative ; top: 100px">
        <strong>GABIM!</strong>
        <h1>Produkti nuk u @if(isset($bool1)) krijua per arsye se produkti {{$item_unique[0]['name'] }} ekziston ne listen e produkteve @endif @if(isset($bool2)) ndryshua pasi nuk u krye asnje ndryshim @endif
        </h1>
    </div>
    <h1></h1>
@endsection