@extends('layouts.admin')

@section('content')




    <h1 class="page-header text-center">@if(isset($customer)) Faturat e {{$customer->buyer_name . " " . $customer->buyer_surname}}
        @else Faturat @endif
    </h1>
    <div class="container-fluid">

        {{--<form action="/searchinvoice" method="POST" role="search" style="width: 310px; height: 70px">--}}
        {{--{{ csrf_field() }}--}}
        {{--<div class="input-group" >--}}
        {{--<input type="text" class="form-control" name="invoice_search"--}}
        {{--placeholder="Kerko faturat sipas emrit ose mbiemrit"> <span class="input-group-btn">--}}
        {{--<button type="submit" class="btn btn-default">--}}
        {{--<span class="glyphicon glyphicon-search"></span>--}}
        {{--</button>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</form>--}}

        @if( is_null($invoices[0]))
            <h3 class="text-center">Nuk ka asnje fature</h3>
        @endif
        <table class="table table-hover">
            <thead>
            <tr>
                <th></th>
                <th>Data</th>
                <th>Nr Fatures</th>
                <th>Klienti</th>
                <th>Totali pa TVSH</th>
                <th>Totali me TVSH</th>

            </tr>
            </thead>
            <tbody>
            @if(isset($invoices))

                @foreach($invoices as $invoice)

                    @if(!is_null($invoice->created_at))
                        <tr>
                            <td><a href="/admin/invoices/{{$invoice->id}}" class="btn btn-info" role="button">Shiko Faturen</a></td>
                            <td>{{date_format($invoice->created_at , 'd-m-Y')}}</td>
                            <td>{{$invoice->id}}</td>
                            <td>{{$invoice->customer->buyer_name . " " .$invoice->customer->buyer_surname}}</td>
                            <td>{{number_format($invoice->total_without_tax , 1, '.', ',')}}</td>
                            <td>{{number_format($invoice->total_with_tax , 1, '.', ',')}}</td>
                        </tr>
                    @endif

                @endforeach
            </tbody>
        </table>

        @endif




        @if(isset($invoices))
            <div style="position: relative;">{{$invoices->render()}}</div>
        @endif




    </div>

    <style>
        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }
    </style>
@endsection