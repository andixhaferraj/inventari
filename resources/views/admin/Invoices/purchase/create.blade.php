@extends('layouts.admin')



@section('content')

    <script src="//code.jquery.com/jquery-1.12.0.min.js">
    </script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js">
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>


    {!! Form::open(['method'=>'POST','action'=>'AdminPurchaseInvoicesController@store' , 'files' => true, 'onsubmit' => 'return confirm("Je i sigurt qe do te procedosh me blerjen ?")'])!!}
    {!! csrf_field() !!}
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12" >

                <hr>
                <div class="row"  >
                    <div class="col-xs-12 col-md-3 col-lg-3 ">
                        <div class="panel panel-default" style="height: 90">
                            <div class="panel-heading">Shitesi</div>
                            <div class="panel-body">
                                <strong>Ajet Xhaferraj</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3 col-lg-5" >
                        @include('admin.includes.createFormError')
                    </div>



                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="text-center"><strong>Blerje Produktesh</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td><strong>Nr.</strong></td>
                                    <td><strong>Pershkrimi i Mallit</strong></td>
                                    <td class="text-center"><strong>Sasia</strong></td>

                                </tr>
                                </thead>
                                <tbody class="detail">

                                <tr>
                                    <td>1</td>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::select('name', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}
                                        </div>
                                    </td>

                                    <td class="text-center">
                                        <div class="form-group">
                                            {!! Form::number('quantity',null,['class'=>'form-control']) !!}
                                        </div>
                                    </td>


                                </tr>

                                {{--<tr>--}}
                                {{--<td>1</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name2', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity2',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>3</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name3', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity3',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>4</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name4', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity4',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>5</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name5', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity5',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>6</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name6', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity6',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<tr>--}}
                                {{--<td>7</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name7', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity7',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td>8</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name8', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity8',null,['class'=>'form-control']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}


                                {{--</tr>--}}



                                {{--<tr>--}}
                                {{--<td class="highrow"></td>--}}
                                {{--<td class="highrow"></td>--}}
                                {{--<td class="highrow"></td>--}}
                                {{--<td class="highrow"></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td class="emptyrow"></td>--}}
                                {{--<td class="emptyrow"></td>--}}
                                {{--<td class="emptyrow"></td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td class="emptyrow"><i></i></td>--}}
                                {{--<td class="emptyrow"></td>--}}
                                {{--<td class="emptyrow"></td>--}}
                                {{--<td class="emptyrow"></td>--}}



                                {{--</tr>--}}
                                </tbody>
                            </table>
                            <input type="button" value="+" id="add" class="btn btn-primary">

                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="form-group">
            {!! Form::submit('Kliko per te shtuar faturen' , ['class'=>'btn btn-info']) !!}
        </div>

        {!! Form::close() !!}


    </div>


    <script type="text/javascript">

        var cnt = 1 ;


        // document.getElementById("form-style").addEventListener("click", addnewrow);

        $('#add').click(function()
        {
            addnewrow();
        });

        function addnewrow()
        {


            var n=($('.detail tr').length-0)+1;

            cnt++ ;


            if(cnt === 2){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name2" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity2',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 3){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name3" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity3',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 4){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name4" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity4',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }


            if(cnt === 5){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name5" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity5',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }


            if(cnt === 6){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name6" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity6',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 7){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name7" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity7',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 8){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name8" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity8',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 9){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name9" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity9',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 10){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name10" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity10',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 11){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name11" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity11',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 12){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name12" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity12',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 13){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name13" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity13',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 14){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name14" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity14',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt === 15){
                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name15" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity15',null,['class'=>'form-control']) !!} </div> </td>' +
                    '</tr>'
                ;
                $('.detail').append(tr);
            }

            if(cnt > 15){
                window.alert('Nuk mund te vendosesh me shume se 15 produkte ne nje fature te vetme')
            }
        }


    </script>


    <style>
        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }
    </style>

    @endsection
    </div>


