@extends('layouts.admin')

@section('content')

    <script src="//code.jquery.com/jquery-1.12.0.min.js">
    </script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js">
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>


    <h1 class="page-header">Faturat</h1>
    <div class="container-fluid">





        @if(!is_null($invoice->created_at))
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <i class="fa fa-search-plus pull-left icon"></i>
                        <h2>Fatura Nr. {{$invoice->id}}</h2>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
                            <div class="panel panel-default height">
                                <div class="panel-heading">Bleresi</div>
                                <div class="panel-body">
                                    <strong>{{$invoice->customer->buyer_name . " " .$invoice->customer->buyer_surname}}</strong>
                                    <br>{{$invoice->customer->address}}
                                    <br>NIPT : {{$invoice->customer->nipt}}<br>
                                    <br><strong>{{$invoice->customer->buyer_phone_number}}</strong><br>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 col-lg-3 pull-right">
                            <div class="panel panel-default height">
                                <div class="panel-heading">Shitesi</div>
                                <div class="panel-body">
                                    <strong>Ajet Xhaferraj</strong>
                                    <br>
                                    <strong>0692238630</strong><br>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="text-center"><strong>Fature Shitje Tatimore</strong></h3>
                            <h5>Data: {{date_format($invoice->created_at,'d-m-Y')}}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <td><strong>Nr.</strong></td>
                                        <td><strong>Pershkrimi i Mallit</strong></td>
                                        <td class="text-center"><strong>Njesia e Matjes</strong></td>
                                        <td class="text-center"><strong>Sasia</strong></td>
                                        <td class="text-right"><strong>Cmimi per njesi pa TVSH</strong></td>
                                        <td class="text-right"><strong>Vlefta pa TVSH</strong></td>
                                        <td class="text-right"><strong>Vlefta e TVSH</strong></td>
                                        <td class="text-right"><strong>Vlefta me TVSH</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invoice->items as $item)
                                        <tr>
                                            <td>{{$nr++}}</td>
                                            <td>{{$item->name}}</td>
                                            <td class="text-center">{{$item->unit}}</td>
                                            <td class="text-center">{{number_format($item->pivot->quantity , 1, '.', ',')}}</td>
                                            <td class="text-right">{{number_format($item->pivot->unit_price_without_tax, 1, '.', ',')}}</td>
                                            <td class="text-center">{{number_format($item->pivot->total_without_tax , 1, '.', ',')}}</td>
                                            <td class="text-center">{{number_format($item->pivot->tax , 1, '.', ',')}}</td>
                                            <td class="text-right">{{number_format($item->pivot->total_with_tax , 1, '.', ',')}}</td>
                                        </tr>
                                    @endforeach
                                    <?php $nr = 1 ?>
                                    <tr>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow"></td>
                                        <td class="highrow text-center"><strong>Totali pa TVSH</strong></td>
                                        <td class="highrow text-right">{{number_format($invoice->total_without_tax , 1, '.', ',')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow text-center"><strong>Totali TVSH</strong></td>
                                        <td class="emptyrow text-right">{{number_format($invoice->tax_total , 1, '.', ',')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="emptyrow"><i></i></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow"></td>
                                        <td class="emptyrow text-center"><strong>Totali me TVSH</strong></td>
                                        <td class="emptyrow text-right">{{number_format($invoice->total_with_tax , 1, '.', ',')}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::open(['method' => 'DELETE' , 'action' => ['AdminInvoicesController@destroy', $invoice->id] , 'onsubmit' => 'return confirm("Sasia e cdo produkti qe eshte ne kete fature do te shtohet ne inventar nese kjo fature fshihet. Je i sigurt qe do ta fshish faturen?? ")'])!!}
            <div class="form-group">

                {!! Form::submit('FSHI FATUREN' ,  ['class'=>'btn btn-danger' , 'id' => 'deletebutton']) !!}
            </div>
            {!! Form::close() !!}

        @endif








    </div>


    <style>





        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }
    </style>
@endsection