@extends('layouts.admin')



@section('content')

    <script src="//code.jquery.com/jquery-1.12.0.min.js">
    </script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js">
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <div class="container-fluid">

        <div class="col-sm-12"></div>

        {!! Form::open(['method'=>'POST','action'=>'AdminInvoicesController@store' , 'files' => true , 'onsubmit' => 'return confirm("Je i sigurt qe do ta krijosh faturen?")'])!!}
        {!! csrf_field() !!}


        <div class="row">
            <div class="col-sm-12" style="position: relative ; height:150px" >

                <hr>
                <div class="row">
                    <div class="col-xs-12 col-md-3 col-lg-3 pull-left" id="buyer_info" >
                        <div class="panel panel-default" style="height: 105px" >
                            <div class="panel-heading">Bleresi</div>
                            <div class="panel-body">
                                <strong>
                                    <div class="container-fluid">
                                        <div class="form-group">
                                            {!! Form::select('buyer_full_name', [$customer_full_name], null, ['placeholder' => 'Zgjidh Bleresin','class' => 'form-control' ]); !!}
                                        </div>
                                    </div>

                                </strong>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-3 col-lg-6">
                        @include('admin.includes.createFormError')
                    </div>

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="text-center"><strong>Fature Shitje Tatimore</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed" >
                                <thead>
                                <tr>
                                    <td><strong>Nr.</strong></td>
                                    <td><strong>Pershkrimi i Mallit</strong></td>
                                    <td class="text-center"><strong>Sasia</strong></td>
                                    <td class="text-center"><strong>Cmimi</strong></td>
                                    <td class="text-center"><strong>Vlefta pa tvsh</strong></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody class="detail">

                                <tr>
                                    <td class="no">1</td>
                                    <td>
                                        <div class="form-group">
                                            {!! Form::select('name', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}
                                        </div>
                                    </td>

                                    <td class="text-center" id="productquantity">
                                        <div class="form-group">
                                            {!! Form::number('quantity',null,['class'=>'form-control', 'id' => 'number-style']) !!}
                                        </div>
                                    </td>

                                    <td class="text-center" id="productprice">
                                        <div class="form-group" >
                                            {!! Form::text('unit_price_without_tax',null,['class'=>'form-control' , 'id' => 'price-style']) !!}
                                        </div>
                                    </td>

                                    <td class="text-center" id="producttotalprice">

                                    </td>


                                </tr>











                                {{--<tr>--}}
                                {{--<td>2</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name2', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td class="text-center">--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::number('quantity2',null,['class'=>'form-control' , 'id' => 'number-style']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--<td class="text-center">--}}
                                {{--<div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax2',null,['class'=>'form-control' , 'id' => 'price-style']) !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>3</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group">--}}
                                {{--{!! Form::select('name3', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-style','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity3',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax3',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>4</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group" style="width:100">--}}
                                {{--{!! Form::select('name4', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity4',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td class="text-right"><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax4',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>5</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group" style="width:100">--}}
                                {{--{!! Form::select('name5', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-style','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity5',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td class="text-right"><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax5',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>6</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group" style="width:100">--}}
                                {{--{!! Form::select('name6', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-style','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity6',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td class="text-right"><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax6',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>7</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group" style="width:100">--}}
                                {{--{!! Form::select('name7', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-style','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity7',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td class="text-right"><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax7',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}

                                {{--<tr>--}}
                                {{--<td>8</td>--}}
                                {{--<td>--}}
                                {{--<div class="form-group" style="width:100">--}}
                                {{--{!! Form::select('name8', [$item_name], null, ['placeholder' => 'Zgjidh Produktin','class' => 'form-style','class' => 'form-control' , 'id' => 'form-style']); !!}--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--<td> <div class="form-group">--}}
                                {{--{!! Form::number('quantity8',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}
                                {{--<td class="text-right"><div class="form-group" >--}}
                                {{--{!! Form::text('unit_price_without_tax8',null,['class'=>'form-control']) !!}--}}
                                {{--</div></td>--}}

                                {{--</tr>--}}






                                </tbody>

                            </table>

                            <input type="button" value="+" id="add" class="btn btn-primary">

                        </div>


                    </div>
                </div>
            </div>
        </div>



        <div class="form-group">

            {!! Form::submit('Kliko per te shtuar faturen' , ['class'=>'btn btn-info']) !!}

        </div>

        {!! Form::close() !!}

    </div>


    <script type="text/javascript">

        var cnt = 1 ;

        var arr = [];






        $('#add').click(function()
        {
            addnewrow();
        });

        if(cnt === 1){

            var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                '<td class="highrow"></td>' +
                '<td class="highrow"></td>' +
                '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                '<td class="highrow text-center" id = "td1"></td>' +
                '</tr>'
            $('.detail').append(tr);

//
//                    $('#price-style , #number-style , #add').on('keyup click', function(e) {
//
//                        var total = 0 ;
//
//
//                        for(var i = 1 ; i <= 5; i++){
//
//                            if(i === 1){
//
//                                var p = $('#price-style').val();
//                                var q = $('#number-style').val();
//                                var total1 = p * q ;
//                                arr[1] = total1 ;
//                                $('#producttotalprice').html(total1.toLocaleString('en', {useGrouping:true}));
//                                $('#td1').html(total1.toLocaleString('en', {useGrouping:true}));
//
//                            }
//                            else {
//
//                                var tmp = '#price-style' + i ;
//                                var tmp2 = '#number-style' + i ;
//                                var p = $(tmp).val();
//                                var q = $(tmp2).val();
//
//
//
//
//                                if(isNaN(p)) p = 0 ;
//                                if(isNaN(q)) q = 0 ;
//
//                                total = (p*q) + total1 ;
//
//                            }
//
////                            $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
//
//
//                        }
//
//                    });
            var p = $('#price-style').val();
            var q = $('#number-style').val();
            var total1 = p * q ;

            $('#producttotalprice').html(total1.toLocaleString('en', {useGrouping:true}));


            $('#price-style , #number-style , #add').on('keyup click ',function () {


                var total = 0 ;


                for(var i = 1 ; i <= 5; i++){

                    if(i === 1){

                        var p = $('#price-style').val();
                        var q = $('#number-style').val();
                        var total1 = p * q ;
                        arr[1] = total1 ;
                        $('#producttotalprice').html(total1.toLocaleString('en', {useGrouping:true}));
                        $('#td1').html(total1.toLocaleString('en', {useGrouping:true}));

                    }
                    else {

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;
                        var p = $(tmp).val();
                        var q = $(tmp2).val();




                        if(isNaN(p)) p = 0 ;
                        if(isNaN(q)) q = 0 ;

                        total = (p*q) + total1 ;

                    }

//                            $('#td1').html(total.toLocaleString('en', {useGrouping:true}));


                }

            });


        }



        function addnewrow()
        {
            var n=($('.detail tr').length-0);

            cnt++ ;

            if(cnt === 2){

                $('#tr1').remove();


                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name2" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity2',null,['class'=>'form-control' , 'id' => 'number-style2']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax2',null,['class'=>'form-control' , 'id' => 'price-style2']) !!} </div> </td>' +
                    '<td id="producttotalprice2" class="text-center"> </td> + </tr>'
                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id = "td1"></td>' +
                    '</tr>' ;
                $('.detail').append(tr);

                $('#td1').html(arr[1]);
//                        $('#td1').html(tot);

//                        var total = 0 ;
                var p2 = $('#price-style2').val();
                var q2 = $('#number-style2').val();
                var total2 = p2 * q2 ;
                $('#producttotalprice2').html(total2.toLocaleString('en', {useGrouping:true}));

                $('#price-style2 , #number-style2 , #price-style , #add').on('keyup click',function () {


                    var p2 = $('#price-style2').val();
                    var q2 = $('#number-style2').val();
                    var total2 = p2 * q2 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    var total = p * q ;
//                            tot += total ;
                    $('#producttotalprice2').html(total2.toLocaleString('en', {useGrouping:true}));

                    arr[2] = total2 + total ;
                    $('#td1').html(arr[2].toLocaleString('en', {useGrouping:true}));

                });





            }

            if(cnt === 3){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group"> {!! Form::select( "name3" , [$item_name], null, ["placeholder" => "Zgjidh Produktin", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity3',null,['class'=>'form-control' , 'id' => 'number-style3']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax3',null,['class'=>'form-control', 'id' => 'price-style3' ]) !!} </div> </td>' +
                    '<td id="producttotalprice3" class="text-center"> </td> + </tr>'
                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"> </td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[2]);

                $('#price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var p3 = $('#price-style3').val();
                    var q3 = $('#number-style3').val();
                    var total3 = p3 * q3 ;

                    var p2 = $('#price-style2').val();
                    var q2 = $('#number-style2').val();
                    var total2 = p2 * q2 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    var total = p * q ;
//                            tot += total ;
                    $('#producttotalprice3').html(total3.toLocaleString('en', {useGrouping:true}));

                    arr[3] = total2 + total + total3 ;
                    $('#td1').html(arr[3].toLocaleString('en', {useGrouping:true}));
                });

            }

            if(cnt === 4){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" > {!! Form::select( "name4" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity4',null,['class'=>'form-control' , 'id' => 'number-style4']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax4',null,['class'=>'form-control' , 'id' => 'price-style4']) !!} </div> </td>' +
                    '<td id="producttotalprice4" class="text-center"> </td> + </tr>'
                ;
                $('.detail').append(tr);



                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[3]);

                $('#price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 4 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 4){
                            var total4 = k*m ;
                            $('#producttotalprice4').html(total4.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[4] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
                });
            }


            if(cnt === 5){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" > {!! Form::select( "name5" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity5',null,['class'=>'form-control' , 'id' => 'number-style5']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax5',null,['class'=>'form-control' , 'id' => 'price-style5']) !!} </div> </td>' +
                    '<td id="producttotalprice5" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"> </td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[4]);

                $('#price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 5 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 5){
                            var total5 = k*m ;
                            $('#producttotalprice5').html(total5.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[5] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
                });
            }


            if(cnt === 6){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" > {!! Form::select( "name6" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity6',null,['class'=>'form-control' , 'id' => 'number-style6']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax6',null,['class'=>'form-control' , 'id' => 'price-style6']) !!} </div> </td>' +
                    '<td id="producttotalprice6" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"> </td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[5]);

                $('#price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {


                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 6 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 6){
                            var total6 = k*m ;
                            $('#producttotalprice6').html(total6.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[6] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
                });
            }

            if(cnt === 7){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name7" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity7',null,['class'=>'form-control','id' => 'number-style7']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax7',null,['class'=>'form-control','id' => 'price-style7']) !!} </div> </td>' +
                    '<td id="producttotalprice7" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"> </td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[6]);

                $('#price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 7 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 7){
                            var total7 = k*m ;
                            $('#producttotalprice7').html(total7.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[7] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
                });
            }

            if(cnt === 8){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name8" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity8',null,['class'=>'form-control','id' => 'number-style8']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax8',null,['class'=>'form-control','id' => 'price-style8']) !!} </div> </td>' +
                    '<td id="producttotalprice8" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"> </td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[7]);

                $('#price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {


                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 8 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 8){
                            var total8 = k*m ;
                            $('#producttotalprice8').html(total8.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[8] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));
                });
            }

            if(cnt === 9){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name9" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity9',null,['class'=>'form-control','id' => 'number-style9']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax9',null,['class'=>'form-control','id' => 'price-style9']) !!} </div> </td>' +
                    '<td id="producttotalprice9" class="text-center"> </td> + </tr>';

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1" ></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[8]);

                $('#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 9 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 9){
                            var total9 = k*m ;
                            $('#producttotalprice9').html(total9.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[9] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });
            }

            if(cnt === 10){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name10" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity10',null,['class'=>'form-control','id' => 'number-style10']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax10',null,['class'=>'form-control','id' => 'price-style10']) !!} </div> </td>' +
                    '<td id="producttotalprice10" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[9]);

                $('#price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 10 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 10){
                            var total10 = k*m ;
                            $('#producttotalprice10').html(total10.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[10] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));


                });



            }

            if(cnt === 11){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name11" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity11',null,['class'=>'form-control' ,'id' => 'number-style11']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax11',null,['class'=>'form-control' ,'id' => 'price-style11']) !!} </div> </td>' +
                    '<td id="producttotalprice11" class="text-center"> </td> + </tr>' ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[10]);

                $('#price-style11 , #number-style11 , #price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 11 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 11){
                            var total11 = k*m ;
                            $('#producttotalprice11').html(total11.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[11] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });

            }

            if(cnt === 12){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name12" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity12',null,['class'=>'form-control','id' => 'number-style12']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax12',null,['class'=>'form-control','id' => 'price-style12']) !!} </div> </td>' +
                    '<td id="producttotalprice12" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[11]);

                $('#price-style12 , #number-style12 ,#price-style11 , #number-style11 , #price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 12 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 12){
                            var total12 = k*m ;
                            $('#producttotalprice12').html(total12.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[12] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });


            }

            if(cnt === 13){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name13" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity13',null,['class'=>'form-control','id' => 'number-style13']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax13',null,['class'=>'form-control','id' => 'price-style13']) !!} </div> </td>' +
                    '<td id="producttotalprice13" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[12]);

                $('#price-style13 , #number-style13 ,#price-style12 , #number-style12 ,#price-style11 , #number-style11 , #price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 13 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 13){
                            var total13 = k*m ;
                            $('#producttotalprice13').html(total13.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[13] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });
            }

            if(cnt === 14){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name14" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity14',null,['class'=>'form-control','id' => 'number-style14']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax14',null,['class'=>'form-control','id' => 'price-style14']) !!} </div> </td>' +
                    '<td id="producttotalprice14" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[13]);

                $('#price-style14 , #number-style14 ,#price-style13 , #number-style13 ,#price-style12 , #number-style12 ,#price-style11 , #number-style11 , #price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 14 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 14){
                            var total14 = k*m ;
                            $('#producttotalprice14').html(total14.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[14] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });
            }

            if(cnt === 15){

                $('#tr1').remove();

                var tr = '<tr>'+
                    '<td class="no">'+n+'</td>'+
                    '<td><div class="form-group" style="width:100"> {!! Form::select( "name15" , [$item_name], null, ["placeholder" => "Zgjidh Produktin","class" => "form-style", "class" => "form-control" , "id" => "form-style"]); !!} </div></td>'+
                    '<td class="text-center"> <div class="form-group"> {!! Form::number('quantity15',null,['class'=>'form-control','id' => 'number-style15']) !!} </div> </td>' +
                    '<td class="text-center"> <div class="form-group">  {!! Form::text('unit_price_without_tax15',null,['class'=>'form-control','id' => 'price-style15']) !!} </div> </td>' +
                    '<td id="producttotalprice15" class="text-center"> </td> + </tr>'

                ;
                $('.detail').append(tr);

                var tr = '<tr id="tr1"> <td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow"></td>' +
                    '<td class="highrow text-right"><strong>Totali pa TVSH</strong></td>' +
                    '<td class="highrow text-center" id="td1"></td></tr>'
                $('.detail').append(tr);

                $('#td1').html(arr[14]);

                $('#price-style15 , #number-style15 , #price-style14 , #number-style14 ,#price-style13 , #number-style13 ,#price-style12 , #number-style12 ,#price-style11 , #number-style11 , #price-style10 , #number-style10 ,#price-style9 , #number-style9 , #price-style8 , #number-style8 , #price-style7 , #number-style7 , #price-style6 , #number-style6 , #price-style5 , #number-style5 , #price-style4 , #number-style4 , #price-style3 , #number-style3 , #price-style2 , #number-style2 , #price-style , #number-style, #add').on('keyup click ',function () {

                    var total = 0 ;

                    var p = $('#price-style').val();
                    var q = $('#number-style').val();
                    total += p * q ;

                    for(var i = 2 ; i <= 15 ; i++){

                        var tmp = '#price-style' + i ;
                        var tmp2 = '#number-style' + i ;

                        var k = $(tmp).val();
                        var m = $(tmp2).val();

                        if(i === 15){
                            var total15 = k*m ;
                            $('#producttotalprice15').html(total15.toLocaleString('en', {useGrouping:true}));

                        }

                        total += k*m ;

                    }

                    arr[15] = total ;

                    $('#td1').html(total.toLocaleString('en', {useGrouping:true}));

                });


            }

            if(cnt > 15){
                window.alert('Nuk mund te vendosesh me shume se 15 produkte ne nje fature te vetme')
            }

            console.log('cnt = ' + cnt);
        }


    </script>


    <style>




        @media (max-width: 650px) {
            .form-style {

                width: 200px;

            }

            #number-style {

                width: 80px;

            }

            #price-style {

                width: 80px;
                position: relative;
                right: -15px;

            }




        }

        #form-style {

            width: 145px;

        }

        .height {
            min-height: 200px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }
    </style>
@endsection





