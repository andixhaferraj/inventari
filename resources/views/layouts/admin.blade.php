<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inventari</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/libs.css')}}" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="admin-page">

<div id="wrapper">

    <!-- Navigation -->


    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin: 0;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" >
                <span class="sr-only">Toggle navigation </span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <!-- /.navbar-header -->



    {{--<li class="dropdown" style="position: relative ; top:-40px ; left: 10px ;list-style-type:none ; width: 50px ">--}}
    {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
    {{--<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>--}}
    {{--</a>--}}
    {{--<ul class="dropdown-menu dropdown-user">--}}
    {{--<li class="divider"></li>--}}
    {{--<li><a href="{{asset('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Dil </a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--<!-- /.dropdown-user -->--}}
    {{--</li>--}}


    <!-- /.dropdown -->

        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="{{asset('/admin/invoices/create')}}"><i class=""></i>Fature Shitje</a>
                    </li>

                    <li>
                        <a href="{{asset('/admin/items/')}}"><i class=""></i>Inventari</a>
                    </li>

                    <li>
                        <a href="#"><i class="fa arrow"></i>Produktet<span class=""></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{asset('/admin/items/')}}">Shiko Produktet</a>
                            </li>
                            <li>
                                <a href="{{asset('/admin/items/create')}}">Krijo Produkt</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>





                    <li>
                        <a href="#"><i class=""></i> Faturat<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{asset('/admin/invoices/')}}">Shiko Faturat</a>
                            </li>
                            <li>
                                <a href="{{asset('/admin/invoices/create')}}">Krijo Fature Shitje</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->

                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class=""></i> Klientet<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{asset('/admin/customers/')}}">Shiko Klientet</a>
                            </li>
                            <li>
                                <a href="{{asset('/admin/customers/create')}}">Krijo Klient</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="{{asset('admin/purchase/invoices/create')}}"><i class=""></i>Blerje Produktesh<span class=""></span></a>
                    </li>


                    <li>
                        <a href="{{asset('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Dil </a>
                    </li>



                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    @yield('content')

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="{{asset('js/libs.js')}}"></script>

</body>

</html>
