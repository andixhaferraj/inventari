<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    //

    use SoftDeletes;

    protected $fillable = [
        'id',
        'buyer_name',
        'buyer_surname',
        'buyer_phone_number',
        'nipt',
        'address',
        'transporter_name',
        'transporter_surname',
        'transporter_phone_number',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['deleted_at'];

    public function invoices(){
        return $this->hasMany('App\Invoice');
    }
}
