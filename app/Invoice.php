<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'id',
        'customer_id',
        'total_without_tax',
        'total_with_tax',
        'tax_total',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['deleted_at'];

    public function customer(){
        return $this->belongsTo('App\Customer')->withTrashed();
    }


    public function items(){
        return $this->belongsToMany('App\Item')->withPivot(['invoice_id', 'item_id', 'quantity' , 'unit_price_without_tax' ,
                                                                    'total_without_tax' , 'tax' , 'total_with_tax' ]);
    }
}
