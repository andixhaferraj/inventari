<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'id',
        'name',
        'unit',
        'unit_price',
        'quantity',
        'created_at',
        'updated_at'
    ];
    protected $dates = ['deleted_at'];
    public function invoices(){
        return $this->belongsToMany('App\Invoice')->withPivot(['quantity' , 'unit_price_without_tax' , 'total_without_tax' , 'tax' , 'total_with_tax' , 'created_at']);
    }
}
