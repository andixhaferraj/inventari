<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InvoiceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'buyer_full_name' => 'required',
            'name' => 'required',
            'quantity' => 'required|numeric|min:1',
            'unit_price_without_tax' => 'required|numeric|min:0.01|max:999999',
            'quantity2' => 'numeric|min:1',
            'unit_price_without_tax2' => 'numeric|min:0.1|max:999999\'',
            'quantity3' => 'numeric|min:1',
            'unit_price_without_tax3' => 'numeric|min:0.1|max:999999\'',
            'quantity4' => 'numeric|min:1',
            'unit_price_without_tax4' => 'numeric|min:0.1|max:999999\'',
            'quantity5' => 'numeric|min:1',
            'unit_price_without_tax5' => 'numeric|min:0.1|max:999999\'',
            'quantity6' => 'numeric|min:1',
            'unit_price_without_tax6' => 'numeric|min:0.1|max:999999\'',
            'quantity7' => 'numeric|min:1',
            'unit_price_without_tax7' => 'numeric|min:0.1|max:999999\'',
            'quantity8' => 'numeric|min:1',
            'unit_price_without_tax8' => 'numeric|min:0.1|max:999999\'',



        ];
    }
}
