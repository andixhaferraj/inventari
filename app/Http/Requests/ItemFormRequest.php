<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ItemFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|string|max:25',
            'unit' => 'required',
            'quantity' => 'required|numeric|max:999999',
            'unit_price' => 'required|numeric|max:99999'
        ];
    }
}
