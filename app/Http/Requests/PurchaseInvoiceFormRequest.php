<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PurchaseInvoiceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'quantity' => 'required|numeric|min:1',
            'quantity2' => 'numeric|min:1',
            'quantity3' => 'numeric|min:1',
            'quantity4' => 'numeric|min:1',
            'quantity5' => 'numeric|min:1',
            'quantity6' => 'numeric|min:1',
            'quantity7' => 'numeric|min:1',
            'quantity8' => 'numeric|min:1',


        ];
    }
}
