<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'buyer_name' => 'required|alpha|max:25',
            'buyer_surname' => 'required|alpha|max:25',
            'buyer_phone_number' => 'required|numeric|max:99999999999',

        ];
    }
}
