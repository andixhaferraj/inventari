<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerFormRequest;
use App\Invoice;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminCustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customers = Customer::orderBy('id' , 'desc')->paginate('12');

        return view('admin.Customers.index' , compact('customers'));
    }

    public function invoiceFilter($id){


        $invoices = Invoice::where('customer_id' , $id)->orderBy('id' , 'desc')->paginate(10);

        $customer = Customer::findOrFail($id);

        return view('admin.Invoices.index' , compact('invoices','customer'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.Customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerFormRequest $request)
    {
        //
        $input = $request->all();

        $input['buyer_name'] = ucfirst(strtolower($input['buyer_name']));
        $input['buyer_surname'] = ucfirst(strtolower($input['buyer_surname']));
        $input['nipt'] = strtoupper($input['nipt']);

        $customer_unique = Customer::where('buyer_name', $input['buyer_name'])
                                    ->where('buyer_surname', $input['buyer_surname'])
                                    ->where('nipt' , $input['nipt'])
                                    ->where('buyer_phone_number' , $input['buyer_phone_number'])
                                    ->get()->toArray();

        if(sizeof($customer_unique) >= 1){
            $bool1 = 1 ;
            //dd($customer_unique[0]['buyer_name']);
            return view('admin/errors/uniqueCustomerError' , compact('customer_unique' , 'bool1'));

        }

        Customer::create($input);

        return redirect('admin/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer = Customer::findOrFail($id);
        return view('admin.Customers.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customer = Customer::findOrFail($id);
        return view('admin.Customers.edit' , compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerFormRequest $request, $id)
    {
        //
        $customer = Customer::findOrFail($id);
        $input = $request->all();

        $input['buyer_name'] = ucfirst(strtolower($input['buyer_name']));
        $input['buyer_surname'] = ucfirst(strtolower($input['buyer_surname']));

        $customer_unique = Customer::where('buyer_name', $input['buyer_name'])
            ->where('buyer_surname', $input['buyer_surname'])
            ->where('nipt' , $input['nipt'])
            ->where('buyer_phone_number' , $input['buyer_phone_number'])
            ->get()->toArray();

        if(sizeof($customer_unique) >= 1){

            $bool2 = 2;
            //dd($customer_unique[0]['buyer_name']);
            return view('admin/errors/uniqueCustomerError' , compact('customer_unique' , 'bool2'));

        }

        $customer->buyer_name = $input['buyer_name'];
        $customer->buyer_surname = $input['buyer_surname'];
        $customer->buyer_phone_number = $input['buyer_phone_number'];
        $customer->address = $input['address'];
        $customer->nipt = $input['nipt'];



        $customer->save();

        return redirect('admin/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = Customer::findOrFail($id);

        $customer->delete();

        return redirect('admin/customers');

    }
}
