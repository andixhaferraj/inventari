<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminPurchaseInvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $item_name = Item::lists('name','id')->all();

        return view('admin.Invoices.purchase.create' , compact('item_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PurchaseInvoiceFormRequest $request)
    {
        //
        $input = $request->all();


        for($i = 2 ; $i <= 15 ; $i++){

            if (isset($input['name'. $i])){

                $item = Item::find($input['name'. $i]);

                if($input['name'] == $input['name'. $i])
                {
                    return view('admin/errors/uniqueItemError' , compact('item'));
                    dd();
                }
            }



            for($j = $i + 1 ; $j <= 15; $j++){

                if(isset($input['name'. $j])){
                    if($input['name'. $i] == $input['name'. $j] && $input['name'. $i] != "" )
                    {
                        $item = Item::find($input['name' .$i]);

                        return view('admin/errors/uniqueItemError' , compact('item'));
                        dd();
                    }
                }


            }

        }
        $item = Item::find($input['name']);
        $item->quantity = $item->quantity + $input['quantity'];
        $item->save();

        for($i = 2 ; $i <= 15 ; $i++){

            if(isset($input['name'. $i])){
                if($input['quantity'. $i] != "" && $input['name'. $i] != ""){

                    $item = Item::find($input['name'. $i]);
                    $item->quantity = $item->quantity + $input['quantity'. $i] ;
                    $item->save();


                }
            }



        }

        return redirect('admin/items');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
