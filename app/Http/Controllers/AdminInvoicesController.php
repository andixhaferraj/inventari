<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\InvoiceFormRequest;
use App\Invoice;
use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminInvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $nr = 1 ;
        $invoices = Invoice::with('items')->orderBy('id' , 'desc')->paginate(10);

        return view('admin.Invoices.index' , compact('invoices' , 'nr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $item_name = Item::lists('name','id')->all();
        $customers = Customer::all();
        $customer_full_name = array();

        foreach ($customers as $customer)
        {
            $name = $customer->buyer_name ;
            $surname = $customer->buyer_surname;
            $full_name = $name . " " . $surname;
            $customer_full_name[$customer->id] = $full_name;


        }

        return view('admin.Invoices.create' , compact('item_name' , 'customer_full_name','customers' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(InvoiceFormRequest $request)
    {
        //
        $input = $request->all();
//        dd($input);
        $item = Item::find($input['name']);



        // dd((double)$input['unit_price_without_tax']);
//        dd($item->quantity);
        $total_without_tax = $input['quantity'] * (double)$input['unit_price_without_tax'];
        $tax = $total_without_tax * 0.2 ;
        $total_with_tax = $total_without_tax + $tax;

        $invoice_total_without_tax = $total_without_tax ;
        $invoice_tax_total = $tax;
        $invoice_total_with_tax = $total_with_tax;

        //dd($input);
        if($item->quantity < $input['quantity']){

            return view('admin/errors/quantityError' , compact('item'));
            dd();

        }



        for($i = 2 ; $i <= 15 ; $i++){


            if(isset($input['name'. $i])){
                if($input['name'] == $input['name'. $i])
                {
                    return view('admin/errors/uniqueItemError' , compact('item'));
                    dd();
                }

                $item = Item::find($input['name'. $i]);
            }






            for($j = $i + 1 ; $j <= 15; $j++){

                if(isset($input['name'. $j])) {

                    if($input['name'. $i] == $input['name'. $j] && $input['name'. $i] != "" )
                    {
                        $item = Item::find($input['name' .$i]);

                        return view('admin/errors/uniqueItemError' , compact('item'));
                        dd();
                    }
                }





            }

            if (isset($input['quantity'.$i])){
                if($input['quantity'. $i] != ""){
                    if(isset($input['quantity'.$i])){
                        if($item->quantity < $input['quantity'. $i]){

                            return view('admin/errors/quantityError' , compact('item'));
                            dd();

                        }
                    }

                }
            }


        }

        $item = Item::find($input['name']);

        $item->quantity = $item->quantity - $input['quantity'] ;

        $item->save();


        $invoice = Invoice::create(['customer_id' => $input['buyer_full_name']]);

        DB::table('invoice_item')->insert(
            ['invoice_id' => $invoice->id , 'item_id' => $input['name'] , 'quantity' => $input['quantity'] ,
                'unit_price_without_tax' => (double)$input['unit_price_without_tax'] , 'total_without_tax' => $total_without_tax ,
                'tax' => $tax , 'total_with_tax' => $total_with_tax , 'created_at' => date("Y-m-d H:i:s")]
        );


        for($i = 2 ; $i <= 15 ; $i++){

            if(isset($input['name'.$i])){
                if($input['quantity'. $i] != "" && $input['name'. $i] != "" && $input['unit_price_without_tax'. $i] != ''){

                    $total_without_tax = $input['quantity' . $i] * (double)$input['unit_price_without_tax' . $i];
                    $tax = $total_without_tax * 0.2 ;
                    $total_with_tax = $total_without_tax + $tax;
                    $invoice_total_without_tax += $total_without_tax ;
                    $invoice_tax_total += $tax;
                    $invoice_total_with_tax += $total_with_tax;

                    $item = Item::find($input['name'. $i]);
                    $item->quantity = $item->quantity - $input['quantity'. $i] ;
                    $item->save();

                    DB::table('invoice_item')->insert(
                        ['invoice_id' => $invoice->id , 'item_id' => $input['name' . $i] , 'quantity' => $input['quantity' . $i] ,
                            'unit_price_without_tax' => (double)$input['unit_price_without_tax' . $i] , 'total_without_tax' => $total_without_tax ,
                            'tax' => $tax , 'total_with_tax' => $total_with_tax , 'created_at' => date("Y-m-d H:i:s")]
                    );
                }
            }



        }

        DB::table('invoices')->where('id','=', $invoice->id)->update(
            ['total_without_tax' => $invoice_total_without_tax ,
                'tax_total' => $invoice_tax_total , 'total_with_tax' => $invoice_total_with_tax , 'created_at' => date("Y-m-d H:i:s")]
        );



        return redirect('admin/invoices');


        // dd($total_without_tax);
//        $invoice_total_without_tax = $input[]


        // dd($invoice->id);


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $invoice = Invoice::findOrFail($id);
        $nr = 1 ;

        return view('admin.Invoices.show' , compact('invoice','nr'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $invoice = Invoice::findOrFail($id);

        $customer_name = $invoice->customer->buyer_name;
        $customer_surname = $invoice->customer->buyer_surname;
        $item_name = Item::lists('name','id')->all();
        $customer_full_name = $customer_name." " . $customer_surname;
        $item_name_chosen = $invoice->items[0]->name;



        return view('admin.Invoices.edit' , compact('invoice','customer_full_name' , 'item_name' , '$item_name_chosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $invoice = Invoice::findOrFail($id);

        foreach ($invoice->items as $item){

            DB::table('items')->where('id','=', $item->id)->update([
                'quantity' => $item->quantity + $item->pivot->quantity
            ]);
        }
        $invoice->delete();


        return redirect('admin/invoices');
        //
    }
}


