<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemFormRequest;
use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $nr = 1 ;

        $items = Item::orderBy('id' , 'desc')->paginate(12);

        return view('admin.Items.index' , compact('items'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.Items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemFormRequest $request)
    {
        //
        $input = $request->all();
        $input['quantity'] = abs($input['quantity']);
        $input['unit_price'] = abs($input['unit_price']);
        $input['name'] = ucfirst(strtolower($input['name']));

        $item_unique = Item::where('name', $input['name'])
                             ->get()->toArray();

        if(sizeof($item_unique) >= 1){

            $bool1 = 1;
            //dd($customer_unique[0]['buyer_name']);
            return view('admin/errors/duplicateItemError' , compact('item_unique' , 'bool1'));

        }

        Item::create($input);
        return redirect('admin/items');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('admin.items.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Item::findOrFail($id);
        return view('admin.Items.edit' , compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemFormRequest $request, $id)
    {
        //

        $item = Item::findOrFail($id);
        $input = $request->all();

        $input['quantity'] = abs($input['quantity']);
        $input['unit_price'] = abs($input['unit_price']);
        $input['name'] = ucfirst(strtolower($input['name']));

        $item_unique = Item::where('name', $input['name'])
                        ->where('unit_price',$input['unit_price'])
                        ->where('quantity' , $input['quantity'])
                        ->get()->toArray();

        if(sizeof($item_unique) >= 1){

            $bool2 = 2;
            //dd($customer_unique[0]['buyer_name']);
            return view('admin/errors/duplicateItemError' , compact('item_unique' , 'bool2'));

        }

        $item->name = $input['name'];
        $item->quantity = abs($input['quantity']);
        $item->unit = $input['unit'];
        $item->unit_price = abs($input['unit_price']);
        $item->save();

        return redirect('admin/items');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
