<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Invoice;
use App\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/', function () {
    return view('welcome');
});


Route::auth();



Route::group(['middleware' => 'admin'] , function (){
    Route::any('/searchcustomer',function(){
        $customer_search = Input::get ( 'customer_search' );
        $customers = \App\Customer::where('buyer_name','LIKE','%'.$customer_search.'%')->orWhere('buyer_surname','LIKE','%'.$customer_search.'%')->paginate(12);

        if(count($customers) > 0)
            return view('admin.Customers.index' , compact('customers'));
        else return view ('admin.Customers.index' , compact('customers'));
    });

    Route::any('/searchitem',function(){
        $item_search = Input::get ( 'item_search' );
        $items = Item::where('name','LIKE','%'.$item_search.'%')->paginate(12);

        if(count($items) > 0)
            return view('admin.Items.index' , compact('items'));
        else return view ('admin.Items.index' , compact('items'));
    });

//    Route::any('/searchinvoice',function(){
//        $invoice_search = Input::get ( 'invoice_search' );
//
//        if($invoice_search == ""){
//            $invoices = Invoice::with('items')->orderBy('id' , 'desc')->paginate(10);
//            return view('admin.Invoices.index' , compact('invoices'));
//        }
//
//        $customers = \App\Customer::withTrashed()->where('buyer_name','LIKE','%'.$invoice_search.'%')->orWhere('buyer_surname','LIKE','%'.$invoice_search.'%')->paginate(12);
//        $invoice_tot = [];
//        $cnt = 0;
//        foreach ($customers as $customer){
//
//           $invoices = Invoice::where('customer_id','LIKE','%'.$customer->id.'%')->paginate(12);
//
//
//           foreach ($invoices as $invoice){
//               $invoice_tot[$cnt]= $invoice ;
//               $cnt++;
//           }
//        }
//
//    $invoices2 = (object)$invoice_tot;
//
//
//         if(!isset($invoices2)){
//             $invoices2 = Invoice::where('id' , '=' , '1.1')->paginate(10);
//             return view('admin.Invoices.index' , compact('invoices2'));
//
//         }
//
//        if(count($invoices2) > 0)
//            return view('admin.Invoices.index' , compact('invoices2'));
//        else return view ('admin.Invoices.index' , compact('invoices2'));
//    });

    Route::get('/admin/customers/{id}/invoices', [
        'uses' => 'AdminCustomersController@invoiceFilter',
        'as' => 'admin.customers.invoices'
    ]);
    Route::resource('/admin/items' , 'AdminItemsController');
    Route::resource('admin/customers' , 'AdminCustomersController');
    Route::resource('admin/invoices' , 'AdminInvoicesController');
    Route::resource('admin/purchase/invoices/','AdminPurchaseInvoicesController');


});

Route::get('/test', function (){


//        $invoice = Invoice::onlyTrashed()->restore();


    $invoices = Invoice::with('customer')->get();

    foreach ($invoices as $invoice){
        dd($invoice->customer->buyer_name);
    }



//
//    foreach ($invoices as $invoice){
//
//       // dd($invoice->items[0]->name);
//
//        foreach ($invoice->items as $item)
//        {
//            dd($item->pivot->quantity);
//        }
//
//    }

    //dd($invoices);

//    $client_total = DB::table('invoices')
//                ->select(DB::raw('sum(total_with_tax) as customer_total, customer_id '))
//                ->groupBy('customer_id')
//                ->get();
//
//    dd($client_total);

//    $invoice = Invoice::find(76);
////        dd($invoice->items);
//
//    foreach ($invoice->items as $item){
//
//        echo $item->name . " " . $item->pivot->quantity ;
//
//
//        DB::table('items')->where('id','=', $item->id)->update([
//            'quantity' => $item->quantity - $item->pivot->quantity
//        ]);
//
//    }
//
//
//    dd();
//    dd($invoice->items->quantity);


});

Route::get('/home', 'HomeController@index');


//Route::get('/admin/items' , 'AdminItemsController@index');





